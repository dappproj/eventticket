import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { Network, Alchemy } from 'alchemy-sdk';

import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";
import SellNFT from './components/SellNFT';
import Marketplace from './components/Marketplace';
import NFTPage from './components/NFTpage';
const settings = {
  apiKey: "R9YtuFEl9A6oraA_clZqYN4BHj4vIZdQ",
  network: Network.ETH_SEPOLIA,
};

const alchemy = new Alchemy(settings);

const latestBlock = alchemy.core.getBlockNumber();

// Get all outbound transfers for a provided address
alchemy.core
  .getTokenBalances('0x994b342dd87fc825f66e51ffa3ef71ad818b6893')
  .then(console.log);

const nfts = alchemy.nft.getNftsForOwner("vitalik.eth");

alchemy.ws.on(
  { method: "alchemy_pendingTransactions",
  fromAddress: "vitalik.eth" },
  (res) => console.log(res)
);

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Marketplace />}/>
        <Route path="/sellNFT" element={<SellNFT />}/> 
        <Route path="/nftPage/:tokenId" element={<NFTPage />}/>        
      </Routes>
    </BrowserRouter>
  </React.StrictMode>
);
reportWebVitals();
