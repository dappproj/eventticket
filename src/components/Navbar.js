import {

  Link
} from "react-router-dom";
import { useEffect, useState } from 'react';
import { useLocation } from 'react-router';
import '../css/navbar.css';
import Logo from "../components/logo.png";

function Navbar() {
  const [connected, toggleConnect] = useState(false);
  const location = useLocation();
  const [currAddress, updateAddress] = useState('0x');

  async function getAddress() {
    const ethers = require("ethers");
    const provider = new ethers.providers.Web3Provider(window.ethereum);
    const signer = provider.getSigner();
    const addr = await signer.getAddress();
    updateAddress(addr);
  }

  function updateButton() {
    // Remove any styling changes you want to remove
  }

  async function connectWebsite() {
    const chainId = await window.ethereum.request({ method: 'eth_chainId' });
    if (chainId !== '0x5') {
      await window.ethereum.request({
        method: 'wallet_switchEthereumChain',
        params: [{ chainId: '0x5' }],
      })
    }
    await window.ethereum.request({ method: 'eth_requestAccounts' })
      .then(() => {
        updateButton();
        getAddress();
        window.location.replace(location.pathname)
      });
  }

  useEffect(() => {
    if (window.ethereum === undefined)
      return;
    let val = window.ethereum.isConnected();
    if (val) {
      getAddress();
      toggleConnect(val);
      updateButton();
    }

    window.ethereum.on('accountsChanged', function (accounts) {
      window.location.replace(location.pathname)
    })
  }, [location.pathname]);

  return (
    <div>
      <nav>
        <ul>
          <li>
            <Link to="/">
            <img src={Logo} alt="Event Ticket Logo" style={{ height: '100px', width: '300px' }} />


            </Link>
          </li>
          <li>
            <ul>
              {location.pathname === "/" ?
                <li>
                  <Link to="/">Marketplace</Link>
                </li>
                :
                <li>
                  <Link to="/">Marketplace</Link>
                </li>
              }
              {location.pathname === "/sellNFT" ?
                <li>
                  <Link to="/sellNFT">Upload Tickets</Link>
                </li>
                :
                <li>
                  <Link to="/sellNFT">Upload Tickets</Link>
                </li>
              }

              <li>
                <button onClick={connectWebsite}>{connected ? "Connected" : "Connect Wallet"}</button>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
      <div>
        {currAddress !== "0x" ? "Connected to" : "Not Connected. Please login to view Tickets"} {currAddress !== "0x" ? (currAddress.substring(0, 15) + '...') : ""}
      </div>
    </div>
  );
}

export default Navbar;
